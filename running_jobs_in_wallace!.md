To run jobs/codes in wallace, you should make batch files. \
You can make this files in a notepad on your computer or directly in wallace through the commandline using a commandline text editor such as Vim. \
I will first show how to do this with Vim, if you dont wish to use Vim, you can copy and paste the template of the file straight into a notepad text file.

So you are in wallace and want to make a batch file. lets call this file "mybatchjob.sh". Batch files must have the ".sh" extension. To create this file, type:
```
vim mybatchjob.sh
```
You have opened a new file named "mybatchjob.sh" in Vim, and you will see something like:
```
~
~
~
~
~
~
~
"mybatchjob.sh" [New File]                                    0,0-1         All
```
With "~" covering all lines of your terminal window. \
To be able to type something into this file, you need to press "i". Once you have pressed "i", you entered the "insert mode", and you are free to type or paste contents to your file. \
As in the terminal, clicking with the mouse will not move where your cursor is in the text, you need to move with the keyboard arrows(there are many keyboard shortcuts for faster cursor navigation if you feel like learning that!), and Ctrl-c and Ctrl-V do not work inside Vim. \
Copying and pasting options in the terminal might vary depending on the computer. In my computer, I select what I want to copy from the terminal using the mouse, and pressing enter copies the selected text. For pasting, I right click with my mouse, and the copied content is pasted where the cursor, not the mouse, is.\
Back to the batch file, you should paste the following content in the beginning of your batch files:
```
#!/bin/bash

#SBATCH --job-name=JobNameOfYourChoice
#SBATCH --ntasks=28 # num of cores
#SBATCH --nodes=1
#SBATCH --time=10:00:00 # in hours
#SBATCH --mem=120G
#SBATCH --error=ErrorFileNameOfYourChoice.%J.err
#SBATCH --output=OutputFileNameOfYourChoice.%J.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=youremail@evolbio.mpg.de
#SBATCH --partition=standard
```
the main things to change as a beginner are:
1. the job name
2. how much time this job can run for (!IMPORTANT! your job will be TERMINATED once it exceeds the time you setted up. If you know your job might run for days or weeks, be sure to put enough hours so that your job doesnt get terminated mid-run) 
3. how you want to name your error files. keep the ".%J.", it will add the job number after the file name, so that if you re run the same code, you still have different error and output files. 
4. how you want to name your output file. this is NOT the same as the desired output from the code you decide to run. most of the times this file will show any prompts or information that your code would print to the terminal. it helps seeing if there where problems in the running of the code or where your job run might be stuck at the moment (if the job is still running while you check the output file)
5. the email to which you want to receive updates on your job run (when the job starts running, when it finishes, if it gets cancelled, if it gets terminated, or if it stops running because of an error.)
6. the partition. if the cluster is full, it is worth it to use "sinfo" to check which partitions have idle nodes, so that you dont need to wait a long time before your job starts running. 

after those lines, you can add the code you want to run in the cluster. example:
```
#!/bin/bash

#SBATCH --job-name=JobNameOfYourChoice
#SBATCH --ntasks=28 # num of cores
#SBATCH --nodes=1
#SBATCH --time=10:00:00 # in hours
#SBATCH --mem=120G
#SBATCH --error=ErrorFileNameOfYourChoice.%J.err
#SBATCH --output=OutputFileNameOfYourChoice.%J.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=youremail@evolbio.mpg.de
#SBATCH --partition=standard

export PATH=/data/biosoftware/cellranger/cellranger:$PATH

cellranger count --id=1 --fastqs=./FastqFiles/1/ --transcriptome=./references/1_ref

```
in this case, i added "export PATH=/data/biosoftware/cellranger/cellranger:$PATH" before the code because the software "cellranger" is in a specific location in the cluster which needed to be specified. 

Afer writing the code you want, to save your changes to the file and quit vim you must:
1. press ESC
2. type:
```
:wq
```
w stands for write, q stands for quit. \
if you want to quit without saving, you need to type
```
:q!
```
if you want to save without quitting, type
```
:w
```
Now to run your batch file job, type:
```
sbatch mybatchjob.sh
```
you will receive a promt with the number of your job , lets pretend the number is 6513438. \
To see the status of the jobs you are running, type
```
squeue -u yourusername
```
to cancel a job you sent, type "scancel" followed by the number of the job you want to cancel. in this example we will use the job number 6513438. 
```
scancel 6513438
```
you can type "squeue -u yourusername" again to check that the job you cancelled is no longer in queue. 

