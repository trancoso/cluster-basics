First of all: are you at work, or are you home? If you are home, you need to use the Check Point VPN client (can ask the IT support to install it in your work laptop) to the institute net to be able to access the cluster from the command line. \
If you are at work, open the terminal. !!!!ATTENTION: mouse clicking does not change the cursor position when using the terminal, cursor navigation is done using the keyboard arrows or keyboard shortcuts. shortcuts such as ctrl-c and ctrl-v do not work for copying and pasting. Ctrl-c is used to stop a code while it runs. Different computers and terminals might have different shortcuts.!!!!

To access wallace, write ssh followed by your mpg email username followed by @wallace.ebolbio.mpg.de and press enter.
```
ssh username@wallace.evolbio.mpg.de
```
It will ask for your password. You will not see any character appearing as you type your password. Press enter once you have typed your password. If you made a typo, you will receive this message:
```
Permission denied, please try again.
username@wallace.evolbio.mpg.de's password:
```
Once you have accessed Wallace, you will receive a promt similar to:
```
Welcome to the "Wallace" HPC Cluster at the MPI for Evolutionary Biology.

Example Scripts are in /data/examples
Run  "module avail" for a list of available modules.

-bash-4.2$
```
Commands you type will appear after "-bash-4.2$". \
To see all jobs currently running in wallace, type:
```
squeue
```
(press enter after typing that)\
To see which nodes (computers used as server, i.e. place that will run the code or job you want to submit to the cluster) are not in use at the moment, type:
```
sinfo
```
you will see something like:
```
PARTITION   AVAIL  TIMELIMIT  NODES  STATE NODELIST
global*        up   infinite     24    mix fastnode[01-03,05-08],node[01-09,11-12,14-17,19-20]
global*        up   infinite      2  alloc node[10,18]
testing        up       5:00     24    mix fastnode[01-03,05-08],node[01-09,11-12,14-17,19-20]
testing        up       5:00      2  alloc node[10,18]
testing        up       5:00      2   idle highmemnode[01-02]
highmem        up   infinite      2   idle highmemnode[01-02]
standard       up   infinite     17    mix node[01-09,11-12,14-17,19-20]
standard       up   infinite      2  alloc node[10,18]
fast           up   infinite      7    mix fastnode[01-03,05-08]
highmemnew     up   infinite      1  alloc newnode01
highmemnew     up   infinite      4   idle newnode[02-05]
highmem2new    up   infinite      3   idle newnode[03-05]
```
if the state of the partition is "idle", you can use "ssh" to go to the node which is unused at the moment. Otherwise, if you already have ready batch files (more on this in the "running_jobs_in_wallace!.md" file), you can change the partition for a partition that has idle nodes, if that partition still fits the requirements you need for running your code/job. To "ssh" to the wished node you can do:
```
ssh newnode03
```
newnode03 refers to one of the idle nodes of the partition "highmem2new" from the last example.  \

to leave the node, press ctrl-D. If you press ctrl-D again, you will leave wallace and go back to your personal work computer terminal.\




